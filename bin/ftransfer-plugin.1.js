#!/usr/bin/env node

// Shell Script Runner for Cronicle
// Invoked via the 'Shell Script' Plugin
// Copyright (c) 2018 Machindra Kale
// Released under the MIT License


var client = require('scp2');
// var perf = new Perf();
// perf.setScale( 1 ); // seconds
// perf.begin();

	client.scp("/opt/cronicle/data/example.sh", {
	    host: "ec2-54-194-90-148.eu-west-1.compute.amazonaws.com",
	    username:"vecron",
	    password:"vecron",
	    path: "/home/vecron"
	}, function(err) {
		
		console.log("Exception:" +  err);
		
	})
