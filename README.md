Distributed Job Scheduler : VECron

Steps to install:
+ Install docker and docker-compose
+ make a installation dir, /userpath/vecron
+ checkout code from repository. `git clone repopath`. Or if you already have code available skip this step
+ create a `.env` file, and make changes to suit the system.

```
  cp .env.example .env
```
+ run docker containers:
  docker-compose up --build

+ This will start the applcation.  Repeat steps all machines in network.

+ Only one machine can be master, others will be slave so keep this in mind will creating `.env` on all machines.

+ Master machine can be accessed on browser at hostname:port.

